//
//  ConnectPage.swift
//  Inventory Management System
//
//  Created by Tommy Han on 22/7/2020.
//  Copyright © 2020 Tommy Han. All rights reserved.
//

import SwiftUI

struct ConnectPage: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ConnectPage_Previews: PreviewProvider {
    static var previews: some View {
        ConnectPage()
    }
}
