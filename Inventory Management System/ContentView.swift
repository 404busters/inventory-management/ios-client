//
//  ContentView.swift
//  Inventory Management System
//
//  Created by Tommy Han on 3/7/2020.
//  Copyright © 2020 Tommy Han. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView{
            HomePage()
            .tabItem{
                Image(systemName: "house.fill")
                Text("Home")
            }
            ListPage()
                .tabItem {
                    Image(systemName: "list.bullet")
                    Text("List All")
            }
            AddPage()
                .tabItem {
                    Image(systemName: "plus.circle")
                    Text("New Item")
            }
            ConnectPage()
                .tabItem {
                    Image(systemName: "dot.radiowaves.right")
                    Text("Connect")
            }
            SettingPage()
                .tabItem {
                Image(systemName: "gear")
                Text("Settings")
            }
        }
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
