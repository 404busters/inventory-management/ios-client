//
//  HomePage.swift
//  Inventory Management System
//
//  Created by Tommy Han on 13/7/2020.
//  Copyright © 2020 Tommy Han. All rights reserved.
//

import SwiftUI

struct HomePage: View {
    var body: some View {
        VStack {
            SearchBar(text: .constant(""))
            Form{
                Text("Testing text 1")
                Text("Testing Text 2")
            }
        }
    }
}

struct HomePage_Previews: PreviewProvider {
    static var previews: some View {
        HomePage()
    }
}
