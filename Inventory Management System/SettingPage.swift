//
//  SettingPage.swift
//  Inventory Management System
//
//  Created by Tommy Han on 13/7/2020.
//  Copyright © 2020 Tommy Han. All rights reserved.
//

import SwiftUI

struct SettingPage: View {
    var body: some View {
        Form{
            Text("Testing Item")
            Text("Testing Item 2")
        }
    }
}

struct SettingPage_Previews: PreviewProvider {
    static var previews: some View {
        SettingPage()
    }
}
